def solution(arr: list, k: int) -> bool:
    used_nums = set()

    for i in arr:
        second_num = k - i
        if second_num in used_nums:
            return True
        used_nums.add(i)

    return False


if __name__ == '__main__':
    assert solution([10, 15, 3, 7], 17) is True
    print('OK!')
