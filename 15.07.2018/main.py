from functools import reduce
from operator import mul


def solution(arr: list) -> list:
    if len(arr) <= 1:
        return []

    if arr.index(0):
        return zero_shortcut(arr)

    result_arr = [1]

    c = 1
    for i in arr[:-1]:
        c *= i
        result_arr.append(c)

    c = 1
    for i in range(len(arr) - 1, -1, -1):
        result_arr[i] *= c
        c *= arr[i]

    return result_arr


def zero_shortcut(arr: list) -> list:
    zeros_count = arr.count(0)
    result_arr = [0 for _ in arr]

    if zeros_count == 1:
        zero_index = arr.index(0)
        arr[zero_index] = 1
        result_arr[zero_index] = reduce(mul, arr, 1)

    return result_arr


if __name__ == '__main__':
    assert solution([1, 2, 3, 4, 5]) == [120, 60, 40, 30, 24]
    assert solution([3, 2, 1]) == [2, 3, 6]
    print('OK!')
